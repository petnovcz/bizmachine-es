#!/bin/bash

directory="./x*"  
# url="http://localhost:9200/bizmachine/_bulk"
# url=https://46.101.124.162:9200/bizmachine/_bulk
url_list=("https://157.230.22.48:9200/bizmachine/_bulk" "https://134.122.93.143:9200/bizmachine/_bulk" "https://46.101.124.162:9200/bizmachine/_bulk")
password=JL*iRZD+vXXvx5LjmPHH
concurrent_requests=5

send_file() {
  local filename="$1"
  random_index=$((RANDOM % ${#url_list[@]}))
  random_url=${url_list[$random_index]}
  curl -s --cacert http_ca.crt -u elastic:$password -XPOST $random_url -H "Content-Type: application/x-ndjson" -o /dev/null --data-binary "@$filename"
  echo "File sent: $filename"
}

# Initialize variables
file_counter=0
total_files=$(find $directory -maxdepth 1 -type f | wc -l)
progress_bar_width=30

# Start time
start_time=$(date +%s)

echo "Sending files from directory: $directory"
echo "Total files: $total_files"

# Iterate over files in the directory
for file in $directory; do
  if [[ -f "$file" ]]; then
    ((file_counter++))
    
    send_file "$file" &
    
    # Limit the number of concurrent requests
    if ((file_counter % concurrent_requests == 0)); then
      wait
    fi
    
    # Calculate progress
    progress=$(awk "BEGIN { printf \"%.0f\", ($file_counter / $total_files) * 100 }")
    filled_length=$(awk "BEGIN { printf \"%.0f\", ($progress / 100) * $progress_bar_width }")
    remaining_length=$((progress_bar_width - filled_length))
    
    # Print progress bar
    printf "\rProgress: [%-${filled_length}s%-${remaining_length}s] %d%%\n" \
      "${repeat_char:0:filled_length}" "${repeat_char:0:remaining_length}" "$progress"
  fi
done

echo  # Move to the next line after progress bar
echo "All files sent successfully."

# Wait for any remaining background processes to finish
wait

# Calculate total time
end_time=$(date +%s)
duration=$((end_time - start_time))

echo "Total time: $duration seconds."
