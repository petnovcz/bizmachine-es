<?php

namespace App\Factory;

use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use Nette\SmartObject;

class ElasticFactory
{
    use SmartObject;

    public static function createElasticClient(array $urls, string $esUser, string $esPassword): Client
    {
        $client = ClientBuilder::create()
            ->setHosts($urls)
            ->setBasicAuthentication($esUser, $esPassword)
            ->setCABundle(__DIR__ . '/http_ca.crt')
            ->build();

        return $client;
    }

    public static function createElastic2Client(array $urls): Client
    {
        $client = ClientBuilder::create()
            ->setHosts($urls)
            ->build();

        return $client;
    }

}