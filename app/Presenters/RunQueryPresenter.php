<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\Search;
use App\Model\SearchService;
use Nette;

/**
 * Run saved query
 */
final class RunQueryPresenter extends Nette\Application\UI\Presenter
{
    private SearchService $search;

    private array $enums;

    private array $savedQueries;

    public function __construct(SearchService $search)
    {
        $this->search = $search;

        // load all saved queries
        $dir = __DIR__ . '/../Queries';
        $savedQueries = [
            'basic' => [],
            'with_children_1' => [],
            'with_children_2' => [],
            'with_children_3' => [],
            'with_children_4' => [],
            'with_list' => [],
            'fulltext' => [],
        ];
        foreach ($savedQueries as $type => $values) {
            $files = $this->getFiles($dir . '/' . $type);
            $savedQueries[$type] = $files;
        }
        $this->savedQueries = $savedQueries;
    }

    public function actionDefault($type)
    {
        $criteria = [];
        $this->template->criteria = '';
        $criteria = $this->getRandomCriteria($type);

        $response = $this->search->search($criteria);
        $this->template->results = $response;
        $this->setView('default');
    }

    private function getRandomCriteria(string $type): array
    {
        if (isset($this->savedQueries[$type]) && $this->savedQueries[$type]) {
            $savedQuery = array_rand($this->savedQueries[$type]);
            $file = __DIR__ . '/../Queries/' . $type . '/' . $this->savedQueries[$type][$savedQuery];
            $criteriaContents = file_get_contents($file);
            $this->template->criteria = $criteriaContents;
            $criteria = \GuzzleHttp\json_decode($criteriaContents, true);
        }

        return $criteria;
    }

    public function actionExport($type)
    {
        $criteria = [];
        $this->template->criteria = '';
        $criteria = $this->getRandomCriteria($type);

        $response = $this->search->export($criteria);
        $this->template->results = $response;
        $this->setView('default');
    }

    /**
     * Read saved searches from a directory
     * @param $directory
     * @return array
     */
    private function getFiles($directory) {

        $handle = opendir($directory);
        $files = [];

        while (false !== ($item = readdir($handle))) {
            if (str_contains($item, 'json')) {
                $files[] = $item;
            }
        }

        closedir($handle);
        return $files;
    }

}
