<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\Enums;
use App\Model\Search;
use App\Model\SearchService;
use Nette;


final class HomePresenter extends Nette\Application\UI\Presenter
{
    private SearchService $search;

    private Enums $enums;

    private bool $showEsQuery = true;

    public function __construct(SearchService $search, Enums $enums)
    {
        $this->search = $search;
        $this->enums = $enums;
    }

    public function actionDefault()
    {
        $this->template->results = [];
        $this->template->searchQuery = '';
        $this->template->enums = $this->enums;
        $this->template->criteria = [];
        $this->template->dataSets = $this->search->getDataSets();
        $this->template->currentDataSet = $this->search->getCurrentDataSet();
    }

    /**
     * Handle a user search!
     *
     * @return void
     * @throws Nette\Application\AbortException
     */
    public function handleSearch()
    {
        $criteria = $this->getHttpRequest()->getPost();
        $this->template->criteria = $criteria;
        // save criteria
        if (isset($criteria['save']) && $criteria['save']) {
            unset($criteria['save']);
            $this->saveQuery($criteria);
        } else if (isset($criteria['set_default_dataset']) && $criteria['dataset']) {
            $this->search->setDefaultDataSet($criteria['dataset']);
            $this->redirect('this');
        } else {
            if (isset($criteria['export']) && $criteria['export']) {
                unset($criteria['export']);
                $response = $this->search->export($criteria);
            } else {
                $this->template->esQuery = '';
                $response = $this->search->search($criteria);
                if ($this->showEsQuery) {
                    $this->template->esQuery = json_encode($this->search->createEsQuery($criteria));
                }
            }

            $this->template->results = $response;
            $this->template->searchQuery = $criteria['fulltext'] ?? '';

            $this->redrawControl('results');
        }
    }

    /**
     * Save user search for later performance tests
     *
     * @param array $criteria
     * @return void
     */
    private function saveQuery(array $criteria): void
    {
        // check if query contains children filters
        list($parentFilters, $childrenFilters) = $this->search->getQueryFilters($criteria);

        if (isset($parentFilters['list_include']) || isset($parentFilters['list_exclude'])) {
            $dir = __DIR__ . '/../Queries/with_list';
        } else if (isset($parentFilters['fulltext']) && $parentFilters['fulltext']) {
            $dir = __DIR__ . '/../Queries/fulltext';
        } else {
            $childrenCount = count($childrenFilters);
            if ($childrenCount) {
                $dir = __DIR__ . '/../Queries/with_children_' . $childrenCount;
            } else {
                $dir = __DIR__ . '/../Queries/basic';
            }
        }
        $criteriaJson = json_encode($criteria);
        $fileName = substr(md5($criteriaJson), 0, 8) . '.json';

        file_put_contents($dir . '/' . $fileName, $criteriaJson);
    }
}
