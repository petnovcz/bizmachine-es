<?php

declare(strict_types=1);

namespace App\Model;
/**
 * User watched companies repository
 */
class WatchedCompaniesRepository
{
    /**
     * @var array
     */
    private array $userWatchedCompanies = [];

    public function __construct()
    {
        $watched = file_get_contents(__DIR__ . '/watched.json');
        $this->userWatchedCompanies = json_decode($watched, true);
    }

    public function getWatchedCompaniesByUserId(int $userId): array
    {
        if (isset($this->userWatchedCompanies[$userId])) {
            return $this->userWatchedCompanies[$userId];
        } else {
            return [];
        }
    }
}