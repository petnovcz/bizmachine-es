<?php

declare(strict_types=1);

namespace App\Model;

class CompaniesWithNotesRepository
{
    /**
     * @var array
     */
    private array $userCompanies = [];

    public function __construct()
    {
        $noted = file_get_contents(__DIR__ . '/with_notes.json');
        $this->userCompanies = json_decode($noted, true);
    }

    public function getCompaniesByUserId(int $userId): array
    {
        if (isset($this->userCompanies[$userId])) {
            return $this->userCompanies[$userId];
        } else {
            return [];
        }
    }
}