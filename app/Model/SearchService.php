<?php

declare(strict_types=1);

namespace App\Model;

use Elastic\Apm\ElasticApm;
use Elastic\Elasticsearch\Client;

class SearchService
{
    const READ_ALIAS = 'bizmachine_search';

    private array $dataSets = [
        'bizmachine_100k',
        'bizmachine_500k',
        'bizmachine_1m',
        'bizmachine_1.5m',
        'bizmachine_2m',
        'bizmachine',
        'bizmachine_fulltext'
    ];

    private Client $es;
    private FieldsDefinition $fields;

    private string $indexName = self::READ_ALIAS;

    /**
     * Search constructor.
     * @param Client $es
     */
    public function __construct(Client $es, FieldsDefinition $fields)
    {
        $this->es = $es;
        $this->fields = $fields;
    }

    public function getCurrentDataSet(): string
    {
        $currentIndex = $this->es->cat()->aliases(['name' => self::READ_ALIAS, 'h' => 'index', 'format' => 'json']);
        $currentIndex = $currentIndex->asArray();
        if (!$currentIndex) {
            return '';
        } else {
            return $currentIndex[0]['index'];
        }
    }

    public function export(array $criteria): void
    {
        $transaction = ElasticApm::getCurrentTransaction();
        $transaction->context()->setLabel('request_type', 'export');

        $indexName = $this->indexName;
        $pit = $this->es->openPointInTime(['index' => $indexName, 'keep_alive' => '1m']);

        $pitId = $pit->asArray()['id'];

        $esQuery = $this->createEsQuery($criteria);

        $batchSize = 1000;
        $params = [
            'size' => $batchSize,
            'body' => [
                'query' => $esQuery['query']
            ],
            'index' => $indexName,
            'sort' => [
                'uniqueId'
            ],
            '_source' => false,
            'pit' => [
                'id' => $pitId,
                'keep_alive' => '1m'
            ],
            'track_total_hits' => true
        ];

        // initial search
        $response = $this->es->search($params);
        $hits = $response->asArray();
        $total = $hits['hits']['total']['value'];
        $pages = ceil($total/$batchSize);

        session_abort();
        header('Content-Type: text/plain');

        foreach ($hits['hits']['hits'] as $hit) {
            echo $hit['_id'];
            echo "\n";
        }

        for ($i = 0; $i < $pages; $i++) {
            $searchAfterRequest = $params;
            $searchAfterRequest['search_after'] = $hit['sort'];
            $response = $this->es->search($searchAfterRequest);
            $hits = $response->asArray();
            echo "FROM AFTER";
            foreach ($hits['hits']['hits'] as $hit) {
                echo $hit['_id'];
                echo "\n";
            }
            echo count($hits['hits']['hits']);
            if (count($hits['hits']['hits']) == 0) {
                break;
            }
        }
        exit;
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function search(array $criteria): array
    {
        $esQuery = $this->createEsQuery($criteria);
        $params = [
            'body' => [
                'query' => $esQuery['query']
            ],
            'index' => $this->indexName,
            'track_total_hits' => true
        ];

        //echo json_encode($esQuery);exit;
        $response = $this->es->search($params);

        return $response->asArray();
    }

    public function getDataSets(): array
    {
        return $this->dataSets;
    }

    public function setDefaultDataSet(string $dataset)
    {
        $currentDataSet = $this->getCurrentDataSet();
        $actions = [];
        if ($currentDataSet) {
            $actions[] = [
                'remove' => [
                    'index' => $currentDataSet,
                    'alias' => self::READ_ALIAS
                ]
            ];
        }
        $actions[] = [
            'add' => [
                'index' => $dataset,
                'alias' => self::READ_ALIAS
            ]
        ];
        $this->es->indices()->updateAliases( ['body' => ['actions' => $actions]]);
    }

    /**
     * Main method for creating Elastic query
     * @param array $criteria
     * @return array[]
     */
    public function createEsQuery(array $criteria): array
    {
        // create all query filters (conditions)
        list($parentFilters, $childrenFilters) = $this->getQueryFilters($criteria);

        // build ES query
        $childQueryFilters = [];
        foreach ($childrenFilters as $childrenType => $childFilters) {
            if (!isset($childQueryFilters[$childrenType])) {
                $childQueryFilters[$childrenType] = [
                    'has_child' => [
                        'type' => $childrenType,
                        'query' => [
                            'bool' => [
                                'filter' => []
                            ]
                        ]
                    ]
                ];
            }

            foreach ($childFilters as $childFilter) {
                if (isset($childFilter['min_children']) && $childFilter['min_children'] > 0) {
                    $childQueryFilters[$childrenType]['has_child']['min_children'] = $childFilter['min_children'];
                } else if (isset($childFilter['max_children'])) {
                    $childQueryFilters[$childrenType]['has_child']['max_children'] = $childFilter['max_children'];
                } else {
                    if ($childFilter) {
                        $childQueryFilters[$childrenType]['has_child']['query']['bool']['filter'][] = $childFilter;
                    }
                }
            }
        }

        $parentFilters[] = [
            'term' => [
                'join_type' => 'company'
            ]
        ];
        $esQueries = array_merge($parentFilters, array_values($childQueryFilters));
        $esQuery = [
            'query' => [
                'bool' => [
                    'filter' => array_values($esQueries)
                ]
            ]
        ];
        return $esQuery;
    }

    /**
     * Convert search criteria to ES query filters
     *
     * @param array $criteria
     * @return array[]
     */
    public function getQueryFilters(array $criteria): array
    {
        $parentFilters = [];
        $childrenFilters = [];
        foreach ($criteria as $fieldName => $fieldValues) {
            if (isset($this->fields[$fieldName])) {
                $filter = [];
                if (isset($fieldValues['min']) || isset($fieldValues['max'])) {
                    isset($fieldValues['min']) || $fieldValues['min'] = null;
                    isset($fieldValues['max']) || $fieldValues['max'] = null;
                    if ($fieldValues['min'] != '' || $fieldValues['max'] != '') {
                        $fieldValues['min'] == '' && $fieldValues['min'] = null;
                        $fieldValues['max'] == '' && $fieldValues['max'] = null;
                        $filter = call_user_func_array($this->fields[$fieldName]['filter'], $fieldValues);
                    }
                } else {
                    $filter = call_user_func_array($this->fields[$fieldName]['filter'], [$fieldValues]);
                }
                if (!$filter) {
                    continue;
                }

                if (isset($this->fields[$fieldName]['is_child']) && $this->fields[$fieldName]['is_child']) {
                    $childType = $this->fields[$fieldName]['elastic_type'];
                    isset($childrenFilters[$childType]) || $childrenFilters[$childType] = [];
                    $childrenFilters[$childType][$fieldName] = $filter;
                } else {
                    $parentFilters[$fieldName] = $filter;
                }

            } else {
                //dump("field not found", $fieldName);
            }
        }
        return array($parentFilters, $childrenFilters);
    }
}

