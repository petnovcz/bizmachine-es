<?php

declare(strict_types=1);

namespace App\Model;

class FieldsDefinition implements \ArrayAccess
{
    private array $fields;

    private Enums $enums;

    private UserListRepository $userListRepository;

    private WatchedCompaniesRepository $watchedCompaniesRepository;

    private CompaniesWithNotesRepository $companiesWithNotesRepository;

    public function __construct(Enums $enums, UserListRepository $userListRepository, WatchedCompaniesRepository $watchedCompaniesRepository, CompaniesWithNotesRepository $companiesWithNotesRepository)
    {
        $this->userListRepository = $userListRepository;
        $this->watchedCompaniesRepository = $watchedCompaniesRepository;
        $this->companiesWithNotesRepository = $companiesWithNotesRepository;
        $this->enums = $enums;
        $this->fields = $this->getFieldsDefinition();
    }

    public function getFieldsDefinition(): array
    {
        $enums = $this->enums;
        $userListRepository = $this->userListRepository;
        $watchedCompaniesRepository = $this->watchedCompaniesRepository;
        $companiesWithNotesRepository = $this->companiesWithNotesRepository;
        $fields = [
            'basicInfo_address_localityCodes' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'basicInfo.address.localityCodes' => $values
                        ]
                    ];
                }
            ],
            'metrics_metricEmployeesCountCategoryCode' => [
                'elastic_type' => 'company',
                'filter' => function (?int $min, ?int $max) use ($enums): array {
                    if (is_null($min) && is_null($max)) {
                        return [];
                    }

                    $filterCats = [];
                    foreach ($enums['employee_categories'] as $catCode => $catSet) {
                        if ($catSet['bounds']['min'] >= $min && $catSet['bounds']['max'] <= $max) {
                            $filterCats[] = $catCode;
                        } else {
                            if ($catSet['bounds']['min'] <= $min && $catSet['bounds']['max'] >= $max) {
                                $filterCats[] = $catCode;
                            }
                        }
                    }
                    return [
                        'terms' => [
                            'metrics.metricEmployeesCountCategoryCode' => $filterCats
                        ]
                    ];
                }
            ],
            'metrics_metricBalanceTotal' => [
                'elastic_type' => 'company',
                'filter' => function (?int $min, ?int $max): array {
                    return [
                        'range' => [
                            'metrics.metricBalanceTotal' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'companies_watched' => [
                'elastic_type' => 'company',
                'filter' => function() use ($watchedCompaniesRepository): array {
                    $userId = 1;
                    $watchedCompanies = $watchedCompaniesRepository->getWatchedCompaniesByUserId($userId);
                    return [
                        'ids' => [
                            'values' => $watchedCompanies
                        ]
                    ];
                }
            ],
            'companies_with_notes' => [
                'elastic_type' => 'company',
                'filter' => function() use ($companiesWithNotesRepository): array {
                    $userId = 1;
                    $notedCompanies = $companiesWithNotesRepository->getCompaniesByUserId($userId);
                    return [
                        'ids' => [
                            'values' => $notedCompanies
                        ]
                    ];
                }
            ],
            'basicInfo_legalFormCodes' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'basicInfo.legalFormCodes' => $values
                        ]
                    ];
                }
            ],
            'basicInfo_establishedAt' => [
                'elastic_type' => 'company',
                'filter' => function (?string $min, ?string $max): array {
                    if (is_null($min) && is_null($max)) {
                        return [];
                    }
                    if ($min && $max) {
                        $today = new \DateTime();
                        $todayMax = new \DateTime();
                        if ($min) {
                            $toDate = $today->modify('-' . $min . ' years');
                        } else {
                            $toDate = $today;
                        }

                        if ($max) {
                            $fromDate = $todayMax->modify('-' . $max . ' years');
                        } else {
                            $fromDate = $todayMax->modify('-100 years');
                        }
                        return [
                            'range' => [
                                'basicInfo.establishedAt' => [
                                    'gte' => $fromDate->format($fromDate::ATOM),
                                    'lte' => $toDate->format($toDate::ATOM)
                                ]
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'list_include' => [
                'elastic_type' => 'company',
                'filter' => function (array $listIds) use ($userListRepository): array {
                    $ids = [];
                    foreach ($listIds as $listId) {
                        $companies = $userListRepository->getByListId($listId);
                        $ids = array_merge($ids, $companies);
                    }
                    return [
                        'ids' => [
                            'values' => $ids
                        ]
                    ];
                }
            ],
            'list_exclude' => [
                'elastic_type' => 'company',
                'filter' => function (array $listIds) use ($userListRepository): array {
                    $ids = [];
                    foreach ($listIds as $listId) {
                        $companies = $userListRepository->getByListId($listId);
                        $ids = array_merge($ids, $companies);
                    }
                    return [
                        'bool' => [
                            'must_not' => [
                                'ids' => [
                                    'values' => $ids
                                ]
                            ]
                        ]
                    ];
                }
            ],
            'indicators_indicatorActivity' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    $min = min($values);
                    $max = max($values);
                    return [
                        'range' => [
                            'indicators.indicatorActivity' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'indicators_indicatorReachability' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    $min = min($values);
                    $max = max($values);
                    return [
                        'range' => [
                            'indicators.indicatorReachability' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'indicators_indicatorGrowth' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    $min = min($values);
                    $max = max($values);
                    return [
                        'range' => [
                            'indicators.indicatorGrowth' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'naceCodes_primaryNaceCodes' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'naceCodes.primaryNaceCodes' => $values
                        ]
                    ];
                }
            ],
            'basicInfo_selfDeclaredCategoryCodes' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'basicInfo.selfDeclaredCategoryCodes' => $values
                        ]
                    ];
                }
            ],
            'import_export' => [
                'elastic_type' => 'company',
                'filter' => function (bool $isImporterExporter): array {
                    if ($isImporterExporter) {
                        return [
                            'bool' => [
                                'should' => [
                                    [
                                        'term' => [
                                            'metrics.isImporter' => true
                                        ]
                                    ],
                                    [
                                        'term' => [
                                            'metrics.isExporter' => true
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'basicInfo_institutionalSectorCodes' => [
                'elastic_type' => 'company',
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'basicInfo.institutionalSectorCodes' => $values
                        ]
                    ];
                }
            ],
            'metrics_ownershipInternationalPercentage' => [
                'elastic_type' => 'company',
                'filter' => function (int $min, int $max): array {
                    return [
                        'range' => [
                            'metrics.ownershipInternationalPercentage' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'metrics_ownershipIndividualPercentage' => [
                'elastic_type' => 'company',
                'filter' => function (int $min, int $max): array {
                    return [
                        'range' => [
                            'metrics.ownershipIndividualPercentage' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'contacts_phone' => [
                'elastic_type' => 'company',
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'contacts.phone'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'contacts_email' => [
                'elastic_type' => 'company',
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'contacts.email'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'contacts_website' => [
                'elastic_type' => 'company',
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'contacts.website'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'contacts_facebook' => [
                'elastic_type' => 'company',
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'contacts.facebook'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'contacts_linkedin' => [
                'elastic_type' => 'company',
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'contacts.linkedin'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'contacts_twitter' => [
                'elastic_type' => 'company',
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'contacts.twitter'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'vehicle_count' => [
                'elastic_type' => 'vehicle',
                'is_child' => true,
                'filter' => function (int $min, int $max): array {
                    return [
                        'min_children' => $min,
                        'max_children' => $max
                    ];
                }
            ],
            'vehicle_firstEverRegisteredAt' => [
                'is_child' => true,
                'elastic_type' => 'vehicle',
                'filter' => function (string $min, string $max): array {
                    if ($min && $max) {
                        $fromDate = new \DateTime($min);
                        $toDate = new \DateTime($max);
                        return [
                            'range' => [
                                'firstEverRegisteredAt' => [
                                    'gte' => $fromDate->format($fromDate::ATOM),
                                    'lte' => $toDate->format($fromDate::ATOM)
                                ]
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'vehicle_categoryCodes' => [
                'elastic_type' => 'vehicle',
                'is_child' => true,
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'categoryCodes' => $values
                        ]
                    ];
                }
            ],
            'vehicle_makeCodes' => [
                'elastic_type' => 'vehicle',
                'is_child' => true,
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'makeCodes' => $values
                        ]
                    ];
                }
            ],
            'vehicle_modelCodes' => [
                'elastic_type' => 'vehicle',
                'is_child' => true,
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'modelCodes' => $values
                        ]
                    ];
                }
            ],
            'job-posting_categoryCodes' => [
                'elastic_type' => 'job_posting',
                'is_child' => true,
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'categoryCodes' => $values
                        ]
                    ];
                }
            ],
            'job-posting_count' => [
                'elastic_type' => 'job_posting',
                'is_child' => true,
                'filter' => function (int $min, int $max): array {
                    return [
                        'min_children' => $min,
                        'max_children' => $max
                    ];
                }
            ],
            'location_categoryCodes' => [
                'elastic_type' => 'location',
                'is_child' => true,
                'filter' => function (array $values): array {
                    return [
                        'terms' => [
                            'categoryCodes' => $values
                        ]
                    ];
                }
            ],
            'location_count' => [
                'elastic_type' => 'location',
                'is_child' => true,
                'filter' => function (int $min, int $max): array {
                    return [
                        'min_children' => $min,
                        'max_children' => $max
                    ];
                }
            ],
            'metrics_metricRevenueGrowth' => [
                'elastic_type' => 'company',
                'filter' => function (string $min, string $max): array {
                    return [
                        'range' => [
                            'metrics.metricRevenueGrowth' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'metrics_metricNetProfitMargin' => [
                'elastic_type' => 'company',
                'filter' => function (string $min, string $max): array {
                    return [
                        'range' => [
                            'metrics.metricNetProfitMargin' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'metrics_metricEbitMargin' => [
                'elastic_type' => 'company',
                'filter' => function (string $min, string $max): array {
                    return [
                        'range' => [
                            'metrics.metricEbitMargin' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'metrics_metricValueAddedRatio' => [
                'elastic_type' => 'company',
                'filter' => function (string $min, string $max): array {
                    return [
                        'range' => [
                            'metrics.metricValueAddedRatio' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'metrics_metricCapitalIntensityRatio' => [
                'elastic_type' => 'company',
                'filter' => function (string $min, string $max): array {
                    return [
                        'range' => [
                            'metrics.metricCapitalIntensityRatio' => [
                                'gte' => $min,
                                'lte' => $max
                            ]
                        ]
                    ];
                }
            ],
            'person_count' => [
                'elastic_type' => 'person',
                'is_child' => true,
                'filter' => function (int $min, int $max): array {
                    return [
                        'min_children' => $min,
                        'max_children' => $max
                    ];
                }
            ],
            'person_phone' => [
                'elastic_type' => 'person',
                'is_child' => true,
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'phone'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'person_email' => [
                'elastic_type' => 'person',
                'is_child' => true,
                'filter' => function (bool $exists): array {
                    if ($exists) {
                        return [
                            'exists' => [
                                'field' => 'email'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ],
            'fulltext' => [
                'elastic_type' => 'company',
                'filter' => function (string $query): array {
                    if ($query) {
                        return [
                            'multi_match' => [
                                'fields' => [
                                    'basicInfo.nameˆ10',
                                    'basicInfo.alternativeNamesˆ5',
                                    'basicInfo.selfDeclaredDescription',
                                    'children_fulltext.eshop.name',
                                    'children_fulltext.person.name',
                                    'children_fulltext.person.role_name',
                                    'children_fulltext.person.role_text',
                                    'children_fulltext.vehicle.vin'

                                ],
                                'query' => $query,
                                'operator' => 'AND',
                                'type' => 'cross_fields'
                            ]
                        ];
                    } else {
                        return [];
                    }
                }
            ]
        ];
        return $fields;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->fields[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->fields[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
    }

    public function offsetUnset(mixed $offset): void
    {
    }
}
