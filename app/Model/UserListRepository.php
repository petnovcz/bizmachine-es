<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Repository for user lists (company IDs)
 */
class UserListRepository
{
    /**
     * @var array
     */
    private array $userLists = [];

    public function __construct()
    {
        $userLists = file_get_contents(__DIR__ . '/lists.json');
        $this->userLists = json_decode($userLists, true);
    }

    public function getByListId(string $listId): array
    {
        if (isset($this->userLists[$listId])) {
            return $this->userLists[$listId];
        } else {
            return [];
        }
    }
}