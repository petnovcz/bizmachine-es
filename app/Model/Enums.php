<?php

declare(strict_types=1);

namespace App\Model;

class Enums implements \ArrayAccess
{
    private $enums = [];

    public function __construct()
    {
        $enumsFile = __DIR__ . '/../../cz-enumerations/simple/enums.json';
        $enums = file_get_contents($enumsFile);
        $enums = json_decode($enums, true);
        $this->enums = $enums;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->enums[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->enums[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->enums[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->enums[$offset]);
    }
}
