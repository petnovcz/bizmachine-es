BizMachine Elasticsearch playgroud
==================================

This is a simple web application with search form which goal is to test elasticsearch capabilities for bizmachine project.

It contains:
- Simple script to read sample data and prepare bulk request to elastic (for importing data)
- Script to multiply data to create larger datasets
- Simple web page with search form in Elasticsearch with possibility to save queries for performance tests
- Scripts for simulating search traffic  

Requirements
------------

- Web Project is build on Nette 3.1 and requires PHP 8.0


Installation
------------

- Application can connect to existing Elasticsearch cluster 
- First, create an empty index with mapping and settings - both are in app/elastic_index.json
- Then, index sample data, run application in Docker and search using the web form

Data indexation
---------------

First, bulk data needs to be prepared, using bin/prepare_elastic_bulk.php.

	php bin/prepare_elastic_bulk.php > bulk/bulk

Then, split datafile into smaller chunks and index data into Elsticsearch using bulk/index_elastic.sh script

    cd bulk
    split -l100000 bulk
    sh index_elastic.sh

Search data manually
--------------------
Run web application with Docker 

	docker-compose up -d app

And set elasticsearch URL in config/local.neon

Search data automatically
-------------------------
Traffic simulation can be done using bash scrips in bin/ directory. For example:

    bash bin/search_basic.sh

