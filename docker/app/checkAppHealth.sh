#!/usr/bin/env bash

until $(curl -sSf -XGET --insecure 'http://localhost' > /dev/null); do
    printf 'waiting for app to be ready, trying again in 3 seconds \n'
    sleep 3
done

printf 'App is ready\n'
