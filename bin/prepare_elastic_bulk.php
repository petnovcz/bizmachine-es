<?php

declare(strict_types=1);

ini_set('memory_limit', '6g');

//$directory = __DIR__ . '/../../ultimate-filtering/'; // Replace with the actual directory path
//$directory = '/root/sample_data/ultimate-filtering/'; // Replace with the actual directory path

// directory with companies data
$directory = '/root/20230705_cz_full/';
// flags
$debug = false;
$indexChildren = true;

$childrenCounter = [];


if (is_dir($directory)) {
    $iterator = new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

    $parentData = [
        'company-data'
    ];

    $parentDataFile = 'company-data';
    $childrenData = [
        'eshops' => ['elastic_type' => 'eshop', 'elastic_id' => 'eShopUniqueId'],
        'job-postings' => ['elastic_type' => 'job_posting', 'elastic_id' => 'jobUniqueId'],
        'locations' => ['elastic_type' => 'location', 'elastic_id' => 'locationUniqueId'],
        'people' => ['elastic_type' => 'person', 'elastic_id' => 'personUniqueId'],
        'vehicles' => ['elastic_type' => 'vehicle', 'elastic_id' => 'uniqueId']
    ];

    foreach ($childrenData as $childSetting) {
        $childrenCounter[$childSetting['elastic_type']] = 0;
    }

    $parentRecords = [];
    $childrenRecords = [];
    foreach ($iterator as $file) {
        if ($file->isDir()) {
            $curr = $file->getPathname();

            // first parent company data
            $filePath = $curr . '/' . $parentDataFile . '.json';
            if (!file_exists($filePath)) {
                if ($debug) {
                    echo "Skipping " . $filePath . " - missing root company data\n";
                }
                continue;
            }
            $parentDoc = json_decode(file_get_contents($filePath), true);
            if (!isset($parentDoc['uniqueId'])) {
                if ($debug) {
                    echo "Skipping " . $filePath . " - missing root company data ID\n";
                }
                continue;
            }
            $parentDoc['company_parent_id'] = $parentDoc['uniqueId'];
            $parentDoc['_id'] = $parentDoc['uniqueId'];
            $parentDoc['join_type'] = 'company';



            if (!$indexChildren) {
                continue;
            }

            // then children objects
            $childrenDoc = [];
            $docChildrenCounter = [];
            $totalChildrenCount = 0;
            foreach ($childrenData as $childSetting) {
                $childrenDocCounter[$childSetting['elastic_type']] = 0;
            }
            foreach ($childrenData as $childrenDataFile => $elasticType) {
                $filePath = $curr . '/' . $childrenDataFile . '.jsonl';
                if (!file_exists($filePath)) {
                    continue;
                }
                $childDataFile = fopen($filePath, 'r');
                while (($line = fgets($childDataFile)) !== false) {
                    $childDoc = json_decode($line, true);
                    if (!isset($childDoc[$elasticType['elastic_id']])) {
                        if ($debug) {
                            echo "Skipping children " . $filePath . " - " . $childrenDataFile . " - missing elastic ID\n";
                        }
                        break;
                    }
                    $childDoc['_id'] = $childDoc[$elasticType['elastic_id']];
                    $childDoc['join_type'] = [
                        'name' => $elasticType['elastic_type'],
                        'parent' => $parentDoc['_id']
                    ];
                    $childDoc['elastic_type'] = $elasticType['elastic_type'];
                    $childDoc['company_parent_id'] = $parentDoc['_id'];
                    $childrenDoc[] = $childDoc;
                    $childrenDocCounter[$elasticType['elastic_type']]++;
                    $totalChildrenCount++;
                }
            }

            $parentDoc['children_count'] = $childrenDocCounter;
            $parentDoc['total_children_count'] = $totalChildrenCount;

            // children fulltext fields duplicated in parent for faster fulltext search
            foreach ($childrenDoc as $child) {
                if ($child['elastic_type'] == 'vehicle') {
                    if (isset($child['vin'])) {
                        $parentDoc['children_fulltext']['vehicle']['vin'] = $child['vin'];
                    }
                }
                if ($child['elastic_type'] == 'person') {
                    if (isset($child['name'])) {
                        $parentDoc['children_fulltext']['person']['name'] = $child['name'];
                    }
                    foreach ($child['roles'] as $role) {
                        if (isset($role['text'])) {
                            isset($parentDoc['children_fulltext']['person']['role_text']) || $parentDoc['children_fulltext']['person']['role_text'] = '';
                            $parentDoc['children_fulltext']['person']['role_text'] .= $role['text'] . ' ';
                        }
                        if (isset($role['name'])) {
                            isset($parentDoc['children_fulltext']['person']['role_name']) || $parentDoc['children_fulltext']['person']['role_name'] = '';
                            $parentDoc['children_fulltext']['person']['role_name'] .= $role['name'] . ' ';
                        }
                    }
                }
                if ($child['elastic_type'] == 'eshop') {
                    if (isset($child['name'])) {
                        $parentDoc['children_fulltext']['eshop']['name'] = $child['name'];
                    }
                }
            }

            // prepare bulk for parent doc
            toBulk($parentDoc);

            // bulk for children objects
            foreach ($childrenDoc as $child) {
                $childrenCounter[$child['join_type']['name']]++;
                toBulk($child);
            }
        }
    }

} else {
    echo "Invalid directory: $directory";
}

function toBulk(array $record) {
    if (isset($record['join_type']['parent'])) {
        echo '{"index": {"_id": "' . $record['_id'] . '","routing": "' . $record['join_type']['parent'] . '"}}' . "\n";
    } else {
        echo '{"index": {"_id": "' . $record['_id'] . '"}}' . "\n";
    }

    unset($record['_id']);
    //$bulkDoc = ['doc' =>  $record];
    echo json_encode($record) . "\n";
}
