<?php

declare(strict_types=1);

ini_set('memory_limit', '6g');

$filePath = __DIR__ . '/../bulk/bulk'; // Replace with the actual directory path
if (!file_exists($filePath)) {
    die('bulk file to multiply does not exist');
}

$copies = 10;
$firstCopyNum = 20;

$copyOffset = $copies+$firstCopyNum;

for ($copyNum = $firstCopyNum; $copyNum < $copyOffset;$copyNum++) {
    $file = fopen($filePath, 'r');
    $i = 0;
    while (($line = fgets($file)) !== false) {
        $i++;
        $copyPostfix = '-copy-' . $copyNum;
        if ($i % 2 == 1) {
            $bulkAction = json_decode($line, true);
            $id = $bulkAction['index']['_id'] . $copyPostfix;
            $bulkAction['index']['_id'] = $id;
            if (isset($bulkAction['index']['routing'])) {
                $bulkAction['index']['routing'] .= $copyPostfix;
            }
            echo json_encode($bulkAction);
            echo "\n";
            continue;
        }

        // doc
        $doc = json_decode($line, true);
        if (isset($doc['companyUniqueId'])) {
            $doc['companyUniqueId'] .= $copyPostfix;
        }
        if (isset($doc['uniqueId'])) {
            $doc['uniqueId'] .= $copyPostfix;
        }
        $doc['copy'] = true;
        if (isset($doc['name'])) {
            $doc['name'] .= ' copy ' . $copyNum;
        }
        if (isset($doc['company_parent_id'])) {
            $doc['company_parent_id'] .= $copyPostfix;
        }
        if (isset($doc['join_type']) && is_array($doc['join_type'])) {
            $doc['join_type']['parent'] = $doc['join_type']['parent'] . $copyPostfix;
        }
        $doc['copy_num'] = $copyNum;
        echo json_encode($doc);
        echo "\n";
    }
}
