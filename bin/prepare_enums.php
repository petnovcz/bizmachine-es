<?php

declare(strict_types=1);

ini_set('memory_limit', '4g');

require __DIR__ . '/../vendor/autoload.php';

$configurator = App\Bootstrap::boot();
$container = $configurator->createContainer();

$enums = [];
$enumsTarget = __DIR__ . '/../cz-enumerations/simple/enums.json';

$localities = __DIR__ . '/../cz-enumerations/enums/localities.json';
$localitiesJson = json_decode(file_get_contents($localities), true);
$localitiesMap = [];
foreach ($localitiesJson['data'][0]['childItems'] as $item) {
    $localitiesMap[$item['code']] = $item['name']['value'];
}
asort($localitiesMap);
$enums['localities'] = $localitiesMap;

// legal forms
$legal = __DIR__ . '/../cz-enumerations/company/enums/legal-forms.json';
$legalJson = json_decode(file_get_contents($legal), true);
$legalMap = [];
foreach ($legalJson['data'] as $item) {
    $legalMap[$item['code']] = $item['name']['value'];
}
asort($legalMap);
$enums['legal_forms'] = $legalMap;

// risk types
$riskTypes = __DIR__ . '/../cz-enumerations/company/enums/risk-types.json';
$riskTypesJson = json_decode(file_get_contents($riskTypes), true);
$riskTypesMap = [];
foreach ($riskTypesJson['data'] as $item) {
    $riskTypesMap[$item['code']] = $item['name']['value'];
}
asort($riskTypesMap);
$enums['risk_types'] = $riskTypesMap;

// nace
$nace = __DIR__ . '/../cz-enumerations/enums/nace.json';
$naceJson = json_decode(file_get_contents($nace), true);
$naceMap = [];
foreach ($naceJson['data'] as $item) {
    $naceMap[$item['code']] = $item['name']['value'];
}
asort($naceMap);
$enums['nace'] = $naceMap;

// self_declared_categories
$selfDeclaredCategories = __DIR__ . '/../cz-enumerations/company/enums/self-declared-categories.json';
$selfDeclaredCategoriesJson = json_decode(file_get_contents($selfDeclaredCategories), true);
$selfDeclaredCategoriesMap = [];
foreach ($selfDeclaredCategoriesJson['data'] as $item) {
    $selfDeclaredCategoriesMap[$item['code']] = $item['name']['value'];
}
asort($selfDeclaredCategoriesMap);
$enums['self_declared_categories'] = $selfDeclaredCategoriesMap;

// institutional sectos
$sectors = __DIR__ . '/../cz-enumerations/company/enums/institutional-sectors.json';
$sectorsJson = json_decode(file_get_contents($sectors), true);
$sectorsMap = [];
foreach ($sectorsJson['data'] as $item) {
    $sectorsMap[$item['code']] = $item['name']['value'];
}
asort($sectorsMap);
$enums['institutional_sectors'] = $sectorsMap;


// vehicle categories
$vehicleCategory = __DIR__ . '/../cz-enumerations/enums/vehicle/categories.json';
$vehicleCategoryJson = json_decode(file_get_contents($vehicleCategory), true);
$vehicleCategoryMap = [];
foreach ($vehicleCategoryJson['data'] as $item) {
    $vehicleCategoryMap[$item['code']] = $item['name']['value'];
}
asort($vehicleCategoryMap);
$enums['vehicle_category'] = $vehicleCategoryMap;

// vehicle make (brand)
$vehicleMake = __DIR__ . '/../cz-enumerations/enums/vehicle/makes.json';
$vehicleMakeJson = json_decode(file_get_contents($vehicleMake), true);
$vehicleMakeMap = [];
foreach ($vehicleMakeJson['data'] as $item) {
    $vehicleMakeMap[$item['code']] = $item['name']['value'];
}
asort($vehicleMakeMap);
$enums['vehicle_makes'] = $vehicleMakeMap;

// vehicle model
$vehicleModels = __DIR__ . '/../cz-enumerations/enums/vehicle/models.json';
$vehicleModelsJson = json_decode(file_get_contents($vehicleModels), true);
$vehicleModelsMap = [];
foreach ($vehicleModelsJson['data'] as $item) {
    if (!isset($item['childItems'])) {
        continue;
    }
    foreach ($item['childItems'] as $childItem) {
        $vehicleModelsMap[$childItem['code']] = $item['name']['value'] . " - " . $childItem['name']['value'];
    }
}
asort($vehicleModelsMap);
$enums['vehicle_models'] = $vehicleModelsMap;

// job posting categories
$jobCategories = __DIR__ . '/../cz-enumerations/enums/job-posting/categories.json';
$jobCategoriesJson = json_decode(file_get_contents($jobCategories), true);
$jobCategoriesMap = [];
foreach ($jobCategoriesJson['data'] as $item) {
    $jobCategoriesMap[$item['code']] = $item['name']['value'];
}
asort($jobCategoriesMap);
$enums['job_posting_categories'] = $jobCategoriesMap;

// location categories
$locationCategories = [
    "cat00113",
    "cat00047",
    "cat00106",
    "cat00001",
    "cat00114",
    "cat00058",
    "cat00048",
    "cat00005",
    "cat00103",
    "cat00014"];
$locationCategories = array_combine($locationCategories, $locationCategories);
$enums['location_categories'] = $locationCategories;



// employee categories
$employeeCategories = __DIR__ . '/../cz-enumerations/company/enums/employee-categories.json';
$employeeCategoriesJson = json_decode(file_get_contents($employeeCategories), true);
$employeeCategoriesMap = [];
foreach ($employeeCategoriesJson['data'] as $item) {
    if (!isset($item['lowerBound'])) {
        $employeeCategoriesMap[$item['code']] = ['name' => $item['name']['value'], 'bounds' => ['min' => null, 'max' => null]];
    } else {
        if (!isset($item['upperBound'])) {
            $employeeCategoriesMap[$item['code']] = ['name' => $item['name']['value'], 'bounds' => ['min' => $item['lowerBound'], 'max' => 1000000000]];
        } else {
            $employeeCategoriesMap[$item['code']] = ['name' => $item['name']['value'], 'bounds' => ['min' => $item['lowerBound'], 'max' => $item['upperBound']]];
        }
    }
}
asort($employeeCategoriesMap);
$enums['employee_categories'] = $employeeCategoriesMap;

// revenue categories
$revenueCategories = __DIR__ . '/../cz-enumerations/company/enums/revenue-categories.json';
$revenueCategoriesJson = json_decode(file_get_contents($revenueCategories), true);
$revenueCategoriesMap = [];
foreach ($revenueCategoriesJson['data'] as $item) {
    $revenueCategoriesMap[$item['code']] = $item['name']['value'];
}
asort($revenueCategoriesMap);
$enums['revenue_categories'] = $revenueCategoriesMap;

$enumsJson = json_encode($enums);

file_put_contents($enumsTarget, $enumsJson);
