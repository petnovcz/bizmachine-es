#!/bin/bash

currdir="${0%/*}"
source "$currdir"/vars.sh

URL=$app_url"/run-query/export/with_children_1"  # Replace with your desired URL
DELAY=5  # Replace with the desired delay in seconds between each call

while true; do
    response=$(curl -s -o /dev/null -w "%{http_code}" "$URL")
    if [[ $response -eq 200 ]]; then
        echo "Successful response received. Calling export again after $DELAY seconds..."
        sleep "$DELAY"
    else
        echo "Unsuccessful response received. Retrying export in $DELAY seconds..."
        sleep "$DELAY"
    fi
done