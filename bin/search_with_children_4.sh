#!/bin/bash

currdir="${0%/*}"
source "$currdir"/vars.sh

url=$app_url"/run-query/with_children_4"
num_requests=5

# Function to make the GET request
make_request() {
    while true; do
        curl -s "$url" >/dev/null
    done
}

# Loop to create initial parallel requests
for ((i=1; i<=num_requests; i++)); do
    make_request &
done

# Loop to maintain 5 parallel requests
while true; do
    wait -n 2>/dev/null
    make_request &
done
