<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

$configurator = App\Bootstrap::boot();
$container = $configurator->createContainer();

$directory = __DIR__ . '/../bmu_samples/cz'; // Replace with the actual directory path

if (is_dir($directory)) {
    $iterator = new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

    $parentData = [
        'basic_info',
        'bank_accounts',
        'connected_companies',
        'contacts',
        'contract',
        'eshop',
        'event',
        'financial_report',
        'indicators',
        'location',
        'metrics',
        'nace_nodes',
        'owned_companies',
        'owners',
        'real_estate',
        'risks',
        'segments',
        'statutories',
        'subsidy',
        'tender'
    ];

    $childrenData = [
        'job_posting',
        'vehicle'
    ];

    $parentRecords = [];
    $childrenRecords = [];
    foreach ($iterator as $file) {
        if ($file->isDir()) {
            if ($file->getFileName() != 28733274) {
           //     continue;
            }
            $curr = $file->getPathname();

            // first company data
            foreach ($parentData as $idx => $parentDataFile) {
                $filePath = $curr . '/' . $parentDataFile . '.json';

                if (!file_exists($filePath)) {
                    // check if root basic info exists (basic_info.json)
                    if ($parentDataFile == 'basic_info') {
                        // parent data missing - break, otherwise just skip missing data
                        break;
                    } else {
                        continue;
                    }
                }

                $fileData = json_decode(file_get_contents($filePath), true);
                // read parentDoc
                if ($parentDataFile == 'basic_info') {
                    if (!isset($fileData[0]['data']['uniqueId'])) {
                        echo "Skipping " . $filePath . " - missing root data\n";
                        break;
                    }
                    $fileData = $fileData[0];
                    $parentDocId = $fileData['data']['uniqueId'];
                    $parentDoc = $fileData['data'];
                    $parentDoc['_id'] = $parentDocId;
                    $parentDoc['join_type'] = 'company';
                    continue;
                }

                if (!isset($parentDoc)) {
                    echo "Skipping " . $filePath . " - parent doc missing\n";
                }

                // process additional data
                $additionalData = [];
                foreach ($fileData as $fileDataRecord) {
                    $additionalData[] = $fileDataRecord['data'];
                }

                $additionalDataCount = count($additionalData);
                if ($additionalDataCount > 1) {
                    $parentDoc[$parentDataFile] = $additionalData;
                } elseif ($additionalDataCount == 1) {
                    $parentDoc[$parentDataFile] = $additionalData[0];
                }
            }


            // then children objects
            foreach ($childrenData as $idx => $childrenDataFile) {
                $filePath = $curr . '/' . $childrenDataFile . '.json';

                if (!file_exists($filePath)) {
                    continue;
                }

                $fileData = json_decode(file_get_contents($filePath), true);

                // process additional data
                foreach ($fileData as $fileDataRecord) {
                    $childData = $fileDataRecord['data'];
                    $childData['join_type'] = [
                        'name' => $childrenDataFile,
                        'parent' => $parentDoc['_id']
                    ];
                    $childData['_id'] = $childData['uniqueId'];
                    if (isset($childData['name'])) {
                        isset($parentDoc['childrenText']) || $parentDoc['childrenText'] = [];
                        $parentDoc['childrenText'][] = $childData['name'];
                    }
                    $childrenRecords[] = $childData;
                }
            }

            $parentRecords[] = $parentDoc;


        }
    }


   /* foreach ($parentRecords as $par) {
        echo json_encode($par) . "\n";
    }
    foreach ($childrenRecords as $ch) {
        echo json_encode($ch) . "\n";
    }*/

    foreach($parentRecords as $parentRecord) {
        toBulk($parentRecord);
    }

    foreach ($childrenRecords as $childrenRecord) {
        toBulk($childrenRecord);
    }





} else {
    echo "Invalid directory: $directory";
}

function toBulk(array $record) {
    if (!isset($record['_id'])) {
        dump($record);exit;
    }
    if (isset($record['join_type']['parent'])) {
        echo '{"index": {"_id": "' . $record['_id'] . '","routing": "' . $record['join_type']['parent'] . '"}}' . "\n";
    } else {
        echo '{"index": {"_id": "' . $record['_id'] . '"}}' . "\n";
    }

    unset($record['_id']);
    //$bulkDoc = ['doc' =>  $record];
    echo json_encode($record) . "\n";
}
